/*
 Navicat Premium Data Transfer

 Source Server         : localhost5
 Source Server Type    : MySQL
 Source Server Version : 100136
 Source Host           : localhost:3306
 Source Schema         : dropsuite

 Target Server Type    : MySQL
 Target Server Version : 100136
 File Encoding         : 65001

 Date: 29/08/2019 17:33:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Database structure
-- ----------------------------

DROP DATABASE IF EXISTS dropsuite;
CREATE DATABASE dropsuite;

-- ----------------------------
-- Table structure for table_file
-- ----------------------------
DROP TABLE IF EXISTS `table_file`;
CREATE TABLE `table_file` (
  `id_files` int(13) NOT NULL AUTO_INCREMENT,
  `name_files` text,
  `content` text,
  `size` double DEFAULT NULL,
  PRIMARY KEY (`id_files`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
