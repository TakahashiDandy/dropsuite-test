<?php

require_once("../config/config.php");
require_once("../models/Tables_directory.php");
require_once("../controllers/Scandir.php");

$query = new Tables_directory();
$scandir = new Scandir();

if(!empty($config["url_base"]) && is_dir($config["url_base"])){
    $directory = $config["url_base"];
    $subdirectory = array();
    $data = array_diff(scandir($directory),$config["permitted_files"]);
    $scandir->getSubdirectories($scandir->getFirstDirectories($directory,$data));
    $data = $query->select();
    echo $data["content"]." ".$data["total"];
} else{
    echo "this is not a directory path. change your path in config files.";
    exit();
}
