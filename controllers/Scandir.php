<?php

class Scandir
{
    protected $query;
    public function __construct()
    {
        require_once("../config/config.php");
        require_once("../config/Connections.php");
        require_once("../models/Tables_directory.php");
        $this->query = new Tables_directory();
    }

    function getFirstDirectories($directory, $data){
        $subdir = array();
        foreach ($data as $item){
            $filename = $directory."/".$item;
            if(is_dir($filename)){
                $insertdata = array(
                    "filepath" => $filename,
                    "depth" => 1,
                );
                array_push($subdir,$insertdata);
            } else if(is_file($filename)){
                $file = fopen($filename,"r");
                $content = fread($file,filesize($filename));
                $insertdata = array(
                    "name_files" => $filename,
                    "content" => $content,
                    "size" => filesize($filename),
                );
                if($this->query->checkrows($filename) < 1) {
                    $this->query->insert("table_file",$insertdata);
                }
                fclose($file);
            }
        }
        return $subdir;
    }

    function getSubdirectories($subdir){
        for($i =0; $i< count($subdir); $i++){
            $data = array_diff(scandir($subdir[$i]["filepath"]),array(".","..",".DS_Store"));
            foreach($data as $item){
                $filename = $subdir[$i]["filepath"]."/".$item;
                if(is_dir($filename)){
                    $insertdata = array(
                        "filepath" => $filename,
                        "depth" => 1,
                    );
                    array_push($subdir,$insertdata);
                } elseif (is_file($filename)){
                    $file = fopen($filename,"r");
                    $content = fread($file,filesize($filename));
                    $insertdata = array(
                        "name_files" => $filename,
                        "content" => $content,
                        "size" => filesize($filename),
                    );
                    if($this->query->checkrows($filename) < 1) {
                        $this->query->insert("table_file",$insertdata);
                    }
                    fclose($file);
                }
            }
        }
        return $subdir;
    }
}