# Dropsuite Test

Dropsuite Test is a Php Project for crawling files and read the content. (For Development Used Only)

## Installation
> Export `dropsuite.sql` to your Mysql or PhpMyAdmin

> Configure your path from `config.php`

```
$config['permitted_files'] = array(".", "..",".DS_Store");
```

>Configure base url to path for your needed

```
$config['base_url'] = "<folder path>"
```

## Usage

Run your browser and type `http://<yourip>/dropsuite-test/controllers/scandirectory.php`
