<?php

require_once("../config/Connections.php");
Class Tables_directory{
    protected $instance;
    protected $connection;
    function __construct()
    {
        $this->connection = new Connection();
        $this->instance = $this->connection->connect();
    }

    function insert($tablename,$data){
        $insertquery = "(";
        $insertvalues = "(";
        $insertdata = "(";
        $types = "";
        $pos = 0;
        foreach ($data as $key=>$item) {
            $length = count($data);
            if($pos < $length-1){
                $insertquery .= "$key,";
                $insertvalues .= "?,";
                $insertdata .= "'".htmlentities($item)."',";
            } else{
                $insertquery .= "$key";
                $insertvalues.= "?";
                $insertdata.= "'".htmlentities($item)."'";
            }
            $types .= "s";
            $pos++;
        }
        $insertquery .= ")";
        $insertvalues .= ")";
        $insertdata .= ")";
        $sql = "INSERT INTO " . $tablename . " " . $insertquery . " VALUES ". $insertdata;
        $result = mysqli_query($this->instance,$sql);
        if($result){
            return true;
        } else{
            return false;
        }
    }

    function delete($tablename,$data){
        $sql = "DELETE FROM " . $tablename . " WHERE " . $data;
        if($this->conn->query($sql)){
            return true;
        } else{
            return false;
        }
    }

    function select(){
        $sql = "SELECT content, count(content) as total
                    FROM table_file
                    GROUP BY content 
                    ORDER BY count(content) DESC;";
        $result = mysqli_query($this->instance,$sql);
        $data = mysqli_fetch_array($result);
        return $data;
    }

    function checkrows($name){
        $sql = "SELECT content FROM table_file WHERE name_files = '" . $name . "'";
        $result = mysqli_query($this->instance,$sql);
        $data = mysqli_num_rows($result);
        return $data;
    }
}