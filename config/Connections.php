<?php

/**
 * Created by PhpStorm.
 * User: dandyrahadiansyah
 * Date: 02/12/17
 * Time: 17.19
 */

define('HOST', 'localhost'); //Server Host Default
define('USER', 'root'); //Server User Default
define('PASS', '');// Server Password Default yaitu tanpa password
define('DB', 'dropsuite'); //Nama Database yang ingin digunakan

class Connection{
    public $conn;

    public function connect(){
        $this->conn = new mysqli(HOST, USER, PASS, DB);
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        } else{
            return $this->conn;
        }
    }

    public function close(){
        $this->conn->close();
    }
}


